getD <- function (x, ...) {
  UseMethod("getD", x)
}

getD.bdat <- function(x, ...){
  rBDATPRO::getDiameter(x, Hx=1.3, bark=TRUE)
}

df <- rBDATPRO::buildTree(list(spp=1, D1=30, H=27))
class(df) <- "bdat"
class(df)

getD(df)
