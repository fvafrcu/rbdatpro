Dear CRAN Team,
this is a resubmission of package 'rBDATPRO'. I have added the following changes:

* FIXME

Please upload to CRAN.
Best, Christian

# Package rBDATPRO 0.8.0.9000

Reporting is done by packager version 1.8.0


## Test environments
- R Under development (unstable) (2020-11-20 r79451)
   Platform: x86_64-pc-linux-gnu (64-bit)
   Running under: Devuan GNU/Linux 3 (beowulf)
   0 errors | 0 warnings | 2 notes
- win-builder (devel)

## Local test results
- RUnit:
    rBDATPRO_unit_test - 1 test function, 0 errors, 0 failures in 1 checks.
- Testthat:
    
- Coverage by covr:
    rBDATPRO Coverage: 34.27%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for buildTree by 90.
     Exceeding maximum cyclomatic complexity of 10 for plot.datBDAT by 6.
     Exceeding maximum cyclomatic complexity of 10 for getSpeciesCode by 3.
     Exceeding maximum cyclomatic complexity of 10 for getVolume by 1.
- lintr:
    found 234 lints in 2312 lines of code (a ratio of 0.1012).
- cleanr:
    found 36 dreadful things about your code.
- codetools::checkUsagePackage:
    found 18 issues.
- devtools::spell_check:
    found 122 unkown words.
